import { Pokemon } from '@/model/model';

class PokemonService {
  private baseUrl: string;
  private limit: number;

  constructor() {
    this.baseUrl = 'https://pokeapi.co/api/v2/pokemon';
    this.limit = 20;
  }

  public async fetchPokemonList(offset: number = 0): Promise<Pokemon[]> {
    try {
      const response = await fetch(
        `${this.baseUrl}?limit=${this.limit}&offset=${offset}`
      );

      const data = await response.json();
      const pokemonPromises = data.results.map(
        async (pokemon: { name: string; url: string }) => {
          const pokemonData = await fetch(pokemon.url);
          return pokemonData.json();
        }
      );

      const pokemons: Pokemon[] = await Promise.all(pokemonPromises);
      return pokemons;
    } catch (error) {
      console.error('Ошибка запроса:', error);
      throw new Error('Ошибка запроса');
    }
  }
}

export const pokemonService = new PokemonService();
