// Add all services into this directory
import { Pokemon } from '@/model/model';
import { store } from '@/store/store';
import { pokemonService } from '@/transport/transport';

export const catchPokemon = (pokemon: Pokemon): void => {
  // Логика для "поймать" покемона
  pokemon.caught = true;
};

// Начальное значение для выдачи покемонов
let offset = 0;
// Сервис для запроса и добавления в стор
export const loadPokemons = async () => {
  try {
    store.setLoading(true);
    const pokemons = await pokemonService.fetchPokemonList(offset);
    store.addPokemons(pokemons);
    offset += 20;
    store.setLoading(false);
  } catch (error) {
    store.setError(error.message);
    store.setLoading(false);
  }
};
