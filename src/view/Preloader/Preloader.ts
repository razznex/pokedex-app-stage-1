import styles from './Prloader.module.scss';

const root = document.getElementById('root');
export const renderPreloader = () => {
  const preloader = document.createElement('div');
  preloader.className = styles.loader;

  return preloader;
};
