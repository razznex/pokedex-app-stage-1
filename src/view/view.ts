import { store } from '@/store/store';
import { renderPokemonList } from '@/view/pokemonList/PokemonList';
import { loadPokemons } from '@/service/service';
import { renderPreloader } from '@/view/Preloader/Preloader';
import styles from './Main.module.scss';
import { renderHeader } from '@/view/Header/Header';

const root = document.getElementById('root');


// Рендер скелета страницы
export const renderPage = () => {
  renderHeader();
  const main = document.createElement('main');
  main.id = 'main';
  main.className = styles.main;
  root.appendChild(main);
  root.className = styles.root;
};

// Запрос и рендер покемонов
export const initPokemon = async () => {
  await loadPokemons();

  if (store.state.loading) {
    root.appendChild(renderPreloader());
  } else if (store.state.error) {
    root.innerHTML = `<p>Ошибка: ${store.state.error}</p>`;
  } else {
    renderPokemonList(store.state.pokemons);
  }
};

//Добавление покемонов по скролу
export const initializeScrollHandler = () => {
  const handleScroll = async () => {
    if (
      window.innerHeight + window.scrollY >= document.body.offsetHeight - 100 &&
      !store.state.loading
    ) {
      store.setLoadingNewPokemons(true);
      if (store.state.loadingNewPokemons) {
        document.getElementById('main').appendChild(renderPreloader());
      }
      await loadPokemons();
      renderPokemonList(store.state.pokemons);
      store.setLoadingNewPokemons(false);
    }
  };

  window.addEventListener('scroll', handleScroll);
};
