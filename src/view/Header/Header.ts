import styles from './Header.module.scss';

const root = document.getElementById('root');

export const renderHeader = () => {
  const header = document.createElement('header');
  header.id = 'header';
  header.className = styles.header;
  root.appendChild(header);
  root.className = styles.root;

  const title = document.createElement('h1');
  title.textContent = 'Pokédex';
  title.className = styles.header__title;

  header.appendChild(title);
};
