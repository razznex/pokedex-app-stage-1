import { catchPokemon } from '@/service/service';
import styles from './PokemonList.module.scss';
import { Pokemon } from '@/model/model';

export const renderPokemonList = (pokemons: Pokemon[]) => {
  const main = document.getElementById('main');
  const pokemonContainer = document.createElement('section');
  pokemonContainer.className = styles.pokemon;

  main.innerHTML = '';
  pokemons.forEach((pokemon) => {
    const pokemonCard = document.createElement('article');
    pokemonCard.className = styles.card;

    const pokemonImage = document.createElement('img');
    pokemonImage.className = styles.card__image;
    pokemonImage.src = pokemon.sprites.front_default;
    pokemonImage.alt = pokemon.name;

    const pokemonName = document.createElement('p');
    pokemonName.textContent = pokemon.name;
    pokemonName.className = styles.card__title;

    const pokemonId = document.createElement('p');
    pokemonId.textContent = `ID: ${pokemon.id}`;
    pokemonId.className = styles.card__id;

    const spanButton = document.createElement('span');
    spanButton.className = styles.card__span;
    spanButton.textContent = pokemon.caught ? 'Пойман' : 'Поймать!';

    const catchButton = document.createElement('button');
    catchButton.disabled = pokemon.caught;
    catchButton.className = styles.card__button;
    catchButton.addEventListener('click', () => {
      catchPokemon(pokemon);
      spanButton.textContent = 'Пойман';
      catchButton.disabled = true;
      pokemonCard.classList.add(styles.card_catched);
    });

    pokemonCard.appendChild(pokemonImage);
    pokemonCard.appendChild(pokemonName);
    pokemonCard.appendChild(pokemonId);
    pokemonCard.appendChild(catchButton);
    catchButton.appendChild(spanButton);

    main.appendChild(pokemonContainer);
    pokemonContainer.appendChild(pokemonCard);
  });
};
