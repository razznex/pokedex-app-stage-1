import { initializeScrollHandler, initPokemon, renderPage } from '@/view/view';
import '../common/styles/reset/reset.scss';
import '../common/styles/fonts/fonts.scss';

const root = document.getElementById('root');

if (!root) {
  throw new Error('root not found');
}

renderPage();
initPokemon();
initializeScrollHandler();
