import { Pokemon } from '@/model/model';

interface Store {
  pokemons: Pokemon[];
  loading: boolean;
  loadingNew: boolean;
  error: string | null;
}

const initialState: Store = {
  pokemons: [],
  loading: false,
  loadingNew: false,
  error: null,
};

// Установка первоначальных значений при загрузке страницы
export const store = {
  state: {
    pokemons: [] as Pokemon[],
    loading: false,
    loadingNewPokemons: false,
    error: null as string | null,
  },

  setLoading(loading: boolean) {
    this.state.loading = loading;
  },

  setLoadingNewPokemons(loadingNewPokemons: boolean) {
    this.state.loadingNewPokemons = loadingNewPokemons;
  },

  setError(error: string | null) {
    this.state.error = error;
  },

  addPokemons(newPokemons: Pokemon[]) {
    this.state.pokemons = [...this.state.pokemons, ...newPokemons];
  },
};
